﻿using System;
using Microsoft.Office.Interop.Excel;


namespace ThirdAttemptExcel
{
    class Program
    {

        static void Main(string[] args)
        {
            //Create COM Object
            Application excelApp = new Application();

            if (excelApp == null)
            {
                Console.WriteLine("Excel is not installed!");
                return;
            }

            //Create book, sheets and range from the excel sheet
            Workbook excelBook = excelApp.Workbooks.Open(@"C:\Users\Mayuko\Documents\test.xlsx");
            _Worksheet excelSheet = (Worksheet)excelBook.Sheets[1];
            Microsoft.Office.Interop.Excel.Range excelRange = excelSheet.UsedRange;

            //Count row and column of the sheets
            int rows = excelRange.Rows.Count;
            int cols = excelRange.Columns.Count;


            for (int i = 1; i <= rows; i++)
            {
                //add new line
                Console.Write("\r\n");
                for (int j = 1; j <= cols; j++)
                {
                    var cells = (Microsoft.Office.Interop.Excel.Range)excelRange.Cells[i,j];
                    if (cells != null && cells.Value2 != null)
                    {
                        Console.Write(cells.Value2.ToString() + "\t");
                    }
                }
            }
            //quit the excel
            excelApp.Quit();
            //release COM MS excel object
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            Console.ReadLine();
        }
    }
}
